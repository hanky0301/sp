#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <limits.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

FILE* fp_log;
int serial_num[3] = {[0 ... 2] = 1};
int serial_num_finished = 1;
int flag[3] = {0};
int count_doc = 0, count_sig = 0;

void handler(int signo);

void install_signal_handler();

int main(int argc, char* argv[])
{
	fp_log = fopen("sender_log", "w");
	FILE* fp_data = fopen(argv[1], "r");
	if (fp_data == NULL)
		handle_error("fopen test_data");

	install_signal_handler();
	int i, j;
	int cur_time = 0;
	int process_time[3];
	const int limit[3] = {INT_MAX, 10, 3};
	int priority;
	int time, saved_time = 0;
	struct timespec req, rem;
	int finished;

	while (fscanf(fp_data, "%d%d", &priority, &time) != EOF)
	{
		count_doc++;
		for (i = 0; i < time - saved_time; i++)
		{
			rem.tv_sec = 0;
			rem.tv_nsec = 100000000L;
			finished = -1;
			while (finished != 0)
			{
				req = rem;
				finished = nanosleep(&req, &rem);
			}
			cur_time++;
			for (j = 1; j < 3; j++)
				if (flag[j])
				{
					process_time[j]++;
					if (process_time[j] > limit[j])
					{
						fprintf(fp_log, "timeout %d %d\n", j, serial_num[j]);
						fflush(fp_log);
						exit(0);
					}
				}
		}
		switch (priority)
		{
			case 0:
				fprintf(fp_log, "send 0 %d\n", serial_num[0]);
				fflush(fp_log);
				printf("ordinary\n");
				fflush(stdout);
				break;
			case 1:
				fprintf(fp_log, "send 1 %d\n", serial_num[1]);
				fflush(fp_log);
				kill(getppid(), SIGUSR1);
				process_time[1] = 0;
				flag[1] = 1;
				break;
			case 2:
				fprintf(fp_log, "send 2 %d\n", serial_num[2]);
				fflush(fp_log);
				kill(getppid(), SIGUSR2);
				process_time[2] = 0;
				flag[2] = 1;
				break;
		}
		saved_time = time;
	}
	while (count_sig < count_doc)
	{
		rem.tv_sec = 0;
		rem.tv_nsec = 100000000L;
		finished = -1;
		while (finished != 0)
		{
			req = rem;
			finished = nanosleep(&req, &rem);
		}
		cur_time++;
		for (j = 1; j < 3; j++)
			if (flag[j])
			{
				process_time[j]++;
				if (process_time[j] > limit[j])
				{
					fprintf(fp_log, "timeout %d %d\n", j, serial_num[j]);
					fflush(fp_log);
					exit(0);
				}
			}
	}
	return 0;
}

void handler(int signo)
{
	count_sig++;
	switch (signo)
	{
		case SIGINT:
			fprintf(fp_log, "finish 0 %d\n", serial_num_finished);
			fflush(fp_log);
			serial_num[0]++;
			serial_num_finished++;
			break;
		case SIGUSR1:
			fprintf(fp_log, "finish 1 %d\n", serial_num[1]);
			fflush(fp_log);
			serial_num[1]++;
			flag[1] = 0;
			break;
		case SIGUSR2:
			fprintf(fp_log, "finish 2 %d\n", serial_num[2]);
			fflush(fp_log);
			serial_num[2]++;
			flag[2] = 0;
			break;
	}
	return;
}

void install_signal_handler()
{
	struct sigaction act;
	act.sa_handler = handler;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);

	if (sigaction(SIGINT, &act, NULL) < 0)
		handle_error("sigaction for SIGINT");
	if (sigaction(SIGUSR1, &act, NULL) < 0)
		handle_error("sigaction for SIGUSR1");
	if (sigaction(SIGUSR2, &act, NULL) < 0)
		handle_error("sigaction for SIGUSR2");
	return;
}
