#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>
#include <errno.h>

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

FILE* fp;
pid_t pid;
int serial_num[3] = {[0 ... 2] = 1};

void handle_sigusr(int n);

void sig_usr(int signo);

void sig_int(int signo);

void install_signal_handler();

void handle_ordinary_documents(int fd);

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "usage: ./receiver [test_data]\n");
		exit(EXIT_FAILURE);
	}

	install_signal_handler();
	int fd[2];
	fp = fopen("receiver_log", "we");
	if (pipe(fd) < 0)
		handle_error("pipe");
	
	if ((pid = fork()) < 0)
		handle_error("fork");
	else if (pid == 0)
	{
		dup2(fd[1], STDOUT_FILENO);
		close(fd[0]);
		close(fd[1]);
		if (execl("./sender", "sender", argv[1], (char*)0) < 0)
			handle_error("execl");
	}
	close(fd[1]);
	handle_ordinary_documents(fd[0]);
	return 0;
}

void handle_sigusr(int n)
{
	int finished;
	struct timespec req, rem;

	fprintf(fp, "receive %d %d\n", n, serial_num[n]);
	fflush(fp);
	rem.tv_sec = 0;
	rem.tv_nsec = (n == 1 ? 500000000L : 200000000L);
	finished = -1;
	while (finished != 0)
	{
		req = rem;
		finished = nanosleep(&req, &rem);
	}
	fprintf(fp, "finish %d %d\n", n, serial_num[n]);
	fflush(fp);
	serial_num[n]++;
	return;	
}

void handler(int signo)
{
	switch (signo)
	{
		case SIGUSR1:
			handle_sigusr(1);
			kill(pid, SIGUSR1);
			break;
		case SIGUSR2:
			handle_sigusr(2);
			kill(pid, SIGUSR2);
			break;
		case SIGINT:
			kill(pid, SIGKILL);
			wait(NULL);
			fprintf(fp, "terminate\n");
			fflush(fp);
			exit(0);
	}
	return;
}

void install_signal_handler()
{
	struct sigaction act;
	act.sa_handler = handler;
	act.sa_flags = 0;
	act.sa_flags |= SA_RESTART; 

	sigemptyset(&act.sa_mask);
	if (sigaction(SIGUSR1, &act, NULL) < 0)
		handle_error("sigaction for SIGUSR1");

	sigaddset(&act.sa_mask, SIGUSR1);
	if (sigaction(SIGUSR2, &act, NULL) < 0)
		handle_error("sigaction for SIGUSR2");

	sigfillset(&act.sa_mask);
	if (sigaction(SIGINT, &act, NULL) < 0)
		handle_error("sigaction for SIGINT");
	return;
}

void handle_ordinary_documents(int fd)
{
	struct timespec req, rem;
	int finished;
	char buf[10];
	
	while (read(fd, buf, 10) > 0)
	{
		fprintf(fp, "receive 0 %d\n", serial_num[0]);
		fflush(fp);
		rem.tv_sec = 1;
		rem.tv_nsec = 0;
		finished = -1;
		while (finished != 0)
		{
			req = rem;
			finished = nanosleep(&req, &rem);
		}
		fprintf(fp, "finish 0 %d\n", serial_num[0]);
		fflush(fp);
		serial_num[0]++;
		kill(pid, SIGINT);
	}
	fprintf(fp, "terminate\n");
	fflush(fp);
	return;
}
