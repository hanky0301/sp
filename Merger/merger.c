#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct split
{
	int* pos;
	int len;
} Split;

typedef struct splits
{
	Split split1;
	Split split2;
} Splits;

int compare(const void* data1, const void* data2);

void* sort(void* arg);

void sort_splits(int* arr, int n, int seg_size, int segments);

void* merge(void* arg);

void merge_splits(int* arr, int n, int* seg_size, int* segments);

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "usage: ./merger [segment_size]\n");
		exit(EXIT_FAILURE);
	}
	int n;
	scanf("%d", &n);
	int* arr = malloc(n * sizeof(int));
	int seg_size = atoi(argv[1]);
	int segments = (n / seg_size) + (n % seg_size > 0);
	int i;
	for (i = 0; i < n; i++)
		scanf("%d", &arr[i]);
	
	sort_splits(arr, n, seg_size, segments);
	while (segments > 1)
		merge_splits(arr, n, &seg_size, &segments);
	for (i = 0; i < n - 1; i++)
		printf("%d ", arr[i]);
	printf("%d\n", arr[n - 1]);
	free(arr);
	
	return 0;
}

int compare(const void* data1, const void* data2)
{
	int a = *(int*)data1;
	int b = *(int*)data2;
	if (a < b)
		return -1;
	else if (a > b)
		return 1;
	else
		return 0;
}

void* sort(void* arg)
{
	Split* split = arg;
	qsort(split->pos, split->len, sizeof(int), compare);
	printf("Sorted %d elements.\n", split->len);
	return NULL;
}

void sort_splits(int* arr, int n, int seg_size, int segments)
{
	int i;
	pthread_t tid[segments];
	Split split[segments];

	for (i = 0; i < n; i += seg_size)
	{
		split[i / seg_size].pos = arr + i;
		split[i / seg_size].len = (i + seg_size >= n && n % seg_size) ? n % seg_size : seg_size;
		if (pthread_create(&tid[i / seg_size], NULL, sort, &split[i / seg_size]) != 0)
		{
			fprintf(stderr, "can't create thread to sort split\n");
			exit(EXIT_FAILURE);
		}
	}
	for (i = 0; i < segments; i++)
		if (pthread_join(tid[i], NULL) != 0)
		{
			fprintf(stderr, "can't joid thread sorting split\n");
			exit(EXIT_FAILURE);
		}
	return;
}

void* merge(void* arg)
{
	Splits* splits = arg;
	int* split1 = splits->split1.pos;
	int n1 = splits->split1.len;
	int* split2 = splits->split2.pos;
	int n2 = splits->split2.len;

	int i;
	int cur1 = 0, cur2 = 0;
	int* buf = malloc((n1 + n2) * sizeof(int));
	int count_buf = 0;
	int cur_re1, cur_re2;
	int dup = 0;
	int flag = 0;

	while (cur1 < n1 && cur2 < n2)
	{
		if (split1[cur1] < split2[cur2])
		{
			buf[count_buf++] = split1[cur1++];
			flag = 0;
		}
		else if (split1[cur1] > split2[cur2])
		{
			buf[count_buf++] = split2[cur2++];
			flag = 0;
		}
		else
		{
			buf[count_buf++] = split1[cur1++];
			if (flag == 0)
			{
				dup++;
				flag = 1;
			}
		}
	}
	for (cur_re1 = n1 - 1, cur_re2 = n2 - 1; cur_re1 >= cur1; cur_re1--, cur_re2--)
		split2[cur_re2] = split1[cur_re1];
	for (i = 0; i < count_buf; i++)
		split1[i] = buf[i];
	printf("Merged %d and %d elements with %d duplicates.\n", n1, n2, dup);
	free(buf);
	return NULL;
}

void merge_splits(int* arr, int n, int* seg_size, int* segments)
{
	int i;
	int size = *seg_size;
	int segs = *segments;
	pthread_t tid[segs / 2];
	Splits splits[segs / 2];

	for (i = 0; i + size < n; i += size * 2)
	{
		splits[i / (size * 2)].split1.pos = arr + i;
		splits[i / (size * 2)].split1.len = size;
		splits[i / (size * 2)].split2.pos = arr + i + size;
		splits[i / (size * 2)].split2.len = (i + size * 2 >= n && n % size) ? n % size : size;
		if (pthread_create(&tid[i / (size * 2)], NULL, merge, &splits[i / (size * 2)]) != 0)
		{
			fprintf(stderr, "can't create thread to merge splits\n");
			exit(EXIT_FAILURE);
		}
	}
	for (i = 0; i < segs / 2; i++)
		if (pthread_join(tid[i], NULL) != 0)
		{
			fprintf(stderr, "can't join thread merging splits\n");
			exit(EXIT_FAILURE);
		}
	*seg_size = size * 2;
	*segments = segs / 2 + segs % 2;
	return;
}
