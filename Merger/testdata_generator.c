#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char* argv[])
{
	int i;
	int n = atoi(argv[1]);

	srand(time(NULL));
	printf("%d\n", n);
	for (i = 0; i < n; i++)
		printf("%d ", (rand() % 20) * ((rand() % 2)? 1 : -1));
	printf("\n");

	return 0;
}
