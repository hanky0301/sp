#! /bin/csh -f

set n = (100 10000 1000000 10000000)
set test = (100 1w 100w 1000w)
set dir = Inputs

if (! -d $dir) then
	mkdir $dir
endif

@ i = 1
while ($i <= 4)
	./testdata_generator $n[$i] > $dir/$test[$i].in
	sleep 1
	@ i++
end
