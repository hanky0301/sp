#! /bin/csh -f

set n = (100 10000 1000000 10000000)
set test = (100 1w 100w 1000w)
set segs = (100 25 10 5 2 1)
set dir_in = Inputs
set dir_out = Outputs

if (! -d $dir_out) then
	mkdir $dir_out
	@ i = 1
	while ($i <= 4)
		mkdir $dir_out/$test[$i]
		@ i++
	end
endif

@ i = 1
while ($i <= 4)
	rm -f $dir_out/$test[$i]/time.log
	@ i++
end

@ i = 1
while ($i <= 4)
	@ j = 1
	while ($j <= 6)
		echo "segs = $segs[$j]"":" >> $dir_out/$test[$i]/time.log
		@ size = $n[$i] / $segs[$j]
		(time ./merger $size < $dir_in/$test[$i].in \
		> /dev/null) \
		>> $dir_out/$test[$i]/time.log
		@ j++
	end
	echo "results generated for n = $n[$i]"
	@ i++
end
