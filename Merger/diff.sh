#!/bin/csh -f

set out1 = Outputs/$1/$2.out
set out2 = Outputs/$1/$3.out

tail -1 $out1 > tail1
tail -1 $out2 > tail2
diff tail1 tail2
rm -f tail1 tail2
