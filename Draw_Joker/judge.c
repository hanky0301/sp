#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define CARD_NUM 53

typedef struct player
{
	int id;
	int random_key;
	int pid;
	int total_cards;
} Player;

// shuffle cards and 
void shuffle_cards(int card[CARD_NUM]);

// mkfifo from fifo[5][15]
void make_fifoes(char fifo[5][15], char* argv[]);

// read requests from organizer and intiate players' info
int set_player_info(Player player[4]);

// generate random_keys for players
void generate_keys(Player player[4]);

// read players' initial total_cards
void init_total_cards(FILE* fp[5], Player player[4]);

// game starts here, loser_id returned
int play_game(FILE* fp[5], Player player[4]);

// player[i] draws a card from player[j]
void draw_card(int i, int j, FILE* fp[5], Player player[4]);

int main(int argc, char* argv[])
{
	if (argc != 2)
		fprintf(stderr, "usage: ./judge [judge_num]\n");

	int i, j;
	int card[CARD_NUM];
	char fifo[5][15];
	FILE* fp[5];
	Player player[4];
	int pid;
	int loser_id;
	char index[3] = "A";
	char key[10];

	srand(time(NULL));
	while(set_player_info(player))
	{
		shuffle_cards(card);
		make_fifoes(fifo, argv);

		// fork child processes
		int count = 0;
		for (i = 0; i < 4; i++)
		{
			if ((pid = fork()) < 0)
				fprintf(stderr, "fork player error\n");
			else if (pid == 0)
			{
				sprintf(index, "%c", index[0] + i);
				sprintf(key, "%d", player[i].random_key);
				if (execl("./player", "player", argv[1], index, key, (char*)0) < 0)
					fprintf(stderr, "exec error\n");
			}
			player[i].pid = pid;

			if ((fp[i + 1] = fopen(fifo[i + 1], "r+")) == NULL)
			{
				fprintf(stderr, "fopen fifo error\n");
				exit(0);
			}

			// write initial card sets
			for (j = 0; j < (i == 0 ? 14 : 13) ; j++)
			{
				fprintf(fp[i + 1], "%d ", card[count]);
				count++;
			}
			fprintf(fp[i + 1], "\n");
			fflush(fp[i + 1]);
		}

		if ((fp[0] = fopen(fifo[0], "r+")) == NULL)
		{
			fprintf(stderr, "fopen fifo error\n");
			exit(0);
		}
		init_total_cards(fp, player);
		loser_id = play_game(fp, player);

		// clean up
		for (i = 0; i < 4; i++)
		{
			kill(player[i].pid, 9);
			wait(NULL);
		}
		for (i = 0; i < 5; i++)
			if (unlink(fifo[i]) < 0)
				fprintf(stderr, "unlink fifoes error\n");

		char buf[3];
		int len = sprintf(buf, "%d\n", loser_id);
		if (write(STDOUT_FILENO, buf, len) != len)
			fprintf(stderr, "write loser_id error\n");
	}
	return 0;
}

void shuffle_cards(int card[CARD_NUM])
{
	int i, j;
	int count = 0;
	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 13; j++)
		{
			card[count] = j;
			count++;
		}
	}
	card[CARD_NUM - 1] = 0;

	// shuffle
	for (i = 0; i < CARD_NUM - 1; i++) 
	{
		j = i + rand() / (RAND_MAX / (CARD_NUM - i) + 1);
		int tmp = card[i];
		card[i] = card[j];
		card[j] = tmp;
	}

	return;
}

void make_fifoes(char fifo[5][15], char* argv[])
{
	int i;
	char str[5][3] = {"", "_A", "_B", "_C", "_D"};
	for (i = 0; i < 5; i++)
	{
		sprintf(fifo[i], "judge%s%s.FIFO", argv[1], str[i]);
		mkfifo(fifo[i], 0644);
	}
	return;
}

int set_player_info(Player player[4])
{
	char buf[15];
	int n;
	if ((n = read(STDIN_FILENO, buf, 15)) < 0)
		fprintf(stderr, "game_set read error\n");

	buf[n] = '\0';
	if (buf[0] == '0')
		return 0;
	int i = 0;
	char* start = strtok(buf, " ");
	while (start != NULL)
	{
		player[i].id = atoi(start);								// id
		player[i].total_cards = (i == 0 ? 14 : 13);				// total_cards
		i++;

		start = strtok(NULL, " ");
	}
	generate_keys(player);										// random_key
	return 1;
}

void generate_keys(Player player[4])
{
	int i, j;
	player[0].random_key = rand() % 65536;
	for (i = 1; i < 4; i++)
	{
		for (j = 0; j < i; j++)
		{
			player[i].random_key = rand() % 65536;
			if (player[i].random_key == player[j].random_key)
			{
				player[i].random_key = rand() % 65536;
				j = -1;
			}
		}
	}
	return;
}

void init_total_cards(FILE* fp[5], Player player[4])
{
	int i;
	char index[3];
	int key;
	int cards;
	for (i = 0; i < 4; i++)
	{
		fscanf(fp[0], "%s%d%d", index, &key, &cards);
		if (key != player[index[0] - 'A'].random_key)
			i--;
		else
			player[index[0] - 'A'].total_cards = cards;
	}
	return;
}

int play_game(FILE* fp[5], Player player[4])
{
	int i, j;
	int cards_in_game = 0;
	for (i = 0; i < 4; i++)
		cards_in_game += player[i].total_cards;
	
	while (cards_in_game > 1)
	{
		for (i = 0; i < 4; i++)
		{
			if (player[i].total_cards == 0)
				continue;
			if (i != 3)
				j = i + 1;
			else
				j = 0;
			while (player[j].total_cards == 0)
			{
				if (j != 3)
					j++;
				else
					j = 0;
			}
			if (j == i)
				break;
			draw_card(i, j, fp, player);
		}
		cards_in_game = 0;
		for (i = 0; i < 4; i++)
			cards_in_game += player[i].total_cards;
	}
	for (i = 0; i < 4; i++)
		if (player[i].total_cards == 1)
			return player[i].id;
}

void draw_card(int i, int j, FILE* fp[5], Player player[4])
{
	char index[3];
	int key;
	int card_id;
	int card;
	int elim;

	fprintf(fp[i + 1], "< %d\n", player[j].total_cards);
	fflush(fp[i + 1]);
	fscanf(fp[0], "%s%d%d", index, &key, &card_id);
	while (key != player[i].random_key)
		fscanf(fp[0], "%s%d%d", index, &key, &card_id);

	fprintf(fp[j + 1], "> %d\n", card_id);
	fflush(fp[j + 1]);
	player[j].total_cards -= 1;
	fscanf(fp[0], "%s%d%d", index, &key, &card);
	while (key != player[j].random_key)
		fscanf(fp[0], "%s%d%d", index, &key, &card);

	fprintf(fp[i + 1], "%d\n", card);
	fflush(fp[i + 1]);
	fscanf(fp[0], "%s%d%d", index, &key, &elim);
	while (key != player[i].random_key)
		fscanf(fp[0], "%s%d%d", index, &key, &elim);
	player[i].total_cards += (elim ? -1 : 1);

	return;
}
