#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// eliminate pairs if needed, return new total_cards
int eliminate_pair(int card[15], int total_cards, int input_card);

// game starts here, communicate with judge
void play_game(FILE* fp[2], int card[15], int total_cards, char* argv[]);

int main(int argc, char* argv[])
{
	if (argc != 4)
		fprintf(stderr, "usage: ./player [judge_id] [player_index] [random_key]\n");

	int i;
	int card[15];
	int input_card;
	int total_cards;
	int index = argv[2][0] - 'A';
	char fifo[2][15];
	FILE* fp[2];
	sprintf(fifo[0], "judge%s.FIFO", argv[1]);
	sprintf(fifo[1], "judge%s_%s.FIFO", argv[1], argv[2]);
	if ((fp[1] = fopen(fifo[1], "r")) == NULL)
	{
		fprintf(stderr, "fopen fifo error\n");
		exit(0);
	}
	if ((fp[0] = fopen(fifo[0], "w")) == NULL)
	{
		fprintf(stderr, "fopen fifo error\n");
		exit(0);
	}

	// initiate total_cards
	total_cards = 0;
	for (i = 0; i < (index == 0 ? 14 : 13); i++)
	{
		fscanf(fp[1], "%d", &input_card);
		total_cards = eliminate_pair(card, total_cards, input_card);
	}
	/*//
	fprintf(fp[0], "%s %s %d\n", argv[2], "1", total_cards);
	fflush(fp[0]);
	*///
	fprintf(fp[0], "%s %s %d\n", argv[2], argv[3], total_cards);
	fflush(fp[0]);

	srand(time(NULL));
	play_game(fp, card, total_cards, argv);
	return 0;
}

int eliminate_pair(int card[15], int total_cards, int input_card)
{
	int i, j;
	int old = total_cards;
	for (i = 0; i < total_cards; i++)
	{
		if (input_card == card[i])
		{
			for (j = i; j < total_cards - 1; j++)
				card[j] = card[j + 1];
			total_cards--;
			break;
		}
	}
	if (i == old)
	{
		card[total_cards] = input_card;
		total_cards++;
	}
	return total_cards;
}

void play_game(FILE* fp[2], int card[15], int total_cards, char* argv[])
{
	int i;
	int tmp;
	char type[3];
	int input_card;
	int old_total;
	int drew_card;

	while (total_cards != 0)
	{
		fscanf(fp[1], "%s%d", type, &tmp);
		if (type[0] == '<')
		{
			/*//
			fprintf(fp[0], "%s %s %d\n", argv[2], "1", rand() % tmp + 1);
			fflush(fp[0]);
			*///
			fprintf(fp[0], "%s %s %d\n", argv[2], argv[3], rand() % tmp + 1);
			fflush(fp[0]);

			fscanf(fp[1], "%d", &input_card);
			old_total = total_cards;
			total_cards = eliminate_pair(card, total_cards, input_card);
			/*//
			fprintf(fp[0], "%s %s %d\n", argv[2], "1", total_cards > old_total ? 0 : 1);
			fflush(fp[0]);
			*///
			fprintf(fp[0], "%s %s %d\n", argv[2], argv[3], total_cards > old_total ? 0 : 1);
			fflush(fp[0]);
		}
		else
		{
			drew_card = card[tmp - 1];
			for (i = tmp - 1; i < total_cards - 1; i++)
				card[i] = card[i + 1];
			total_cards--;

			/*//
			fprintf(fp[0], "%s %s %d\n", argv[2], "1", drew_card);
			fflush(fp[0]);
			*///
			fprintf(fp[0], "%s %s %d\n", argv[2], argv[3], drew_card);
			fflush(fp[0]);
		}
	}
	return;
}
