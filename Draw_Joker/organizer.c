#include <unistd.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAX_GAMES 1850

typedef struct player
{
	int id;
	int score;
} Player;

// build game_set of all combinations of players, return total_games
int build_game_set(int player_num, char game_set[MAX_GAMES][15]); 

//comp func for qsort
int compare(const void* player1, const void* player2);

// read results and write requests
void talk_to_judges(int judge_num, fd_set* check_set, char game_set[MAX_GAMES][15], int total_games, Player player[20]);

int main(int argc, char* argv[])
{
	if (argc != 3)
		fprintf(stderr, "usage: ./organizer [judge_num] [player_num]\n");
	
	int i, j;
	int judge_num = atoi(argv[1]);
	int player_num = atoi(argv[2]);
	Player player[20];
	
	int fd[15][2][2]; // fds for pipes
	int pid;

	char game_set[MAX_GAMES][15];
	int total_games = build_game_set(player_num, game_set);
	int count; // the game to be assigned
	int len; // the length of game_set[count]

	fd_set check_set;
	FD_ZERO(&check_set);
	
	// initiate player
	for (i = 0; i < 20; i++)
	{
		player[i].id = i + 1;
		player[i].score = 0;
	}
	count = 1;
	for (i = 1; i <= judge_num; i++)
	{
		// fd[i][0][1] for organizer to write
		// fd[i][1][0] for organizer to read
		for (j = 0; j < 2; j++)
			if (pipe(fd[i][j]) < 0) 
				fprintf(stderr, "pipe error\n");

		// fork child process
		if ((pid = fork()) < 0)
			fprintf(stderr, "fork judge error\n");
		else if (pid == 0)
		{
			close(fd[i][0][1]);
			close(fd[i][1][0]);
			dup2(fd[i][0][0], STDIN_FILENO);	// for judges to read
			dup2(fd[i][1][1], STDOUT_FILENO);	// for judges to write
			close(fd[i][0][0]);
			close(fd[i][1][1]);

			char judge_str[5];	// "judge_num"
			sprintf(judge_str, "%d", i);
			if (execl("./judge", "judge", judge_str, (char*)0) < 0)
				fprintf(stderr, "exec error\n");
		}

		close(fd[i][0][0]);
		close(fd[i][1][1]);
		FD_SET(fd[i][1][0], &check_set);

		//write initial game_set
		len = strlen(game_set[count]);
		if (write(fd[i][0][1], game_set[count], len) != len)
			fprintf(stderr, "write initial game_set error\n");
		count++;
	}
	talk_to_judges(judge_num, &check_set, game_set, total_games, player);

	// process output
	qsort(player, player_num, sizeof(Player), compare);
	for (i = 0; i < player_num; i++)
		printf("%d ", player[i].id);
	printf("\n");

	return 0;
}

int build_game_set(int player_num, char game_set[MAX_GAMES][15])
{
	int count = 1;
	int i, j, k, l;
	sprintf(game_set[0], "0 0 0 0\n");
	for (i = 1; i <= player_num - 3; i++)
		for (j = i + 1; j <= player_num - 2; j++)
			for (k = j + 1; k <= player_num - 1; k++)
				for (l = k + 1; l <= player_num; l++)
				{
					sprintf(game_set[count], "%d %d %d %d\n", i, j, k, l);
					count++;
				}
	return count - 1;
}

int compare(const void* player1, const void* player2)
{
	Player* ptr1 = (Player*)player1;
	Player* ptr2 = (Player*)player2;
	if (ptr1->score < ptr2->score)
		return -1;
	else if (ptr1->score > ptr2->score)
		return 1;
	else
		if (ptr1->id < ptr2->id)
			return -1;
		if (ptr1->id > ptr2->id)
			return 1;
		else
			return 0;
}

void talk_to_judges(int judge_num, fd_set* check_set, char game_set[MAX_GAMES][15], int total_games, Player player[20])
{
	int i;
	int judge_alive = judge_num;
	fd_set return_set;
	int fd_read;
	int len;
	int count = judge_num + 1;
	while (judge_alive)
	{
		return_set = *check_set;
		if (select(50, &return_set, NULL, NULL, NULL) == -1)
			fprintf(stderr, "select error\n");
		
		// fd_read ready for organizer to read
		for (fd_read = 3; fd_read < 50; fd_read++)
			if (FD_ISSET(fd_read, &return_set))
				break;
		
		// read result (loser_id)
		char buf[5]; 
		if (read(fd_read, buf, 5) == -1)
			fprintf(stderr, "read result error\n");
		player[atoi(buf) - 1].score -= 1;
		/*//
		for (i = 0; i < atoi(buf); i++)
			fprintf(stderr, "  ");
		fprintf(stderr, "%d\n", atoi(buf));
		*///

		if (count <= total_games)
		{
			len = strlen(game_set[count]);
			if (write(fd_read - 1, game_set[count], len) != len)
				fprintf(stderr, "write further game_set error\n");
			count++;
		}
		else
		{
			// request for the judge to exit, fd[0][1] is fd[1][0] - 1
			len = strlen(game_set[0]);
			if (write(fd_read - 1, game_set[0], len) != len) 
				fprintf(stderr, "write exit request error\n");
			FD_CLR(fd_read, check_set);
			judge_alive--;
		}
	}
	for (i = 0; i < judge_num; i++) 
		wait(NULL);
	return;
}
